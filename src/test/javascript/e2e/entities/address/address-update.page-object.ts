import { element, by, ElementFinder } from 'protractor';

export default class AddressUpdatePage {
  pageTitle: ElementFinder = element(by.id('pizzashopApp.address.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  streetInput: ElementFinder = element(by.css('input#address-street'));
  cityInput: ElementFinder = element(by.css('input#address-city'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setStreetInput(street) {
    await this.streetInput.sendKeys(street);
  }

  async getStreetInput() {
    return this.streetInput.getAttribute('value');
  }

  async setCityInput(city) {
    await this.cityInput.sendKeys(city);
  }

  async getCityInput() {
    return this.cityInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
