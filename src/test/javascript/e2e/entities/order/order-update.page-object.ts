import { element, by, ElementFinder } from 'protractor';

export default class OrderUpdatePage {
  pageTitle: ElementFinder = element(by.id('pizzashopApp.order.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  totalInput: ElementFinder = element(by.css('input#order-total'));
  orderDateInput: ElementFinder = element(by.css('input#order-orderDate'));
  deliveryDateInput: ElementFinder = element(by.css('input#order-deliveryDate'));
  shopAccountSelect: ElementFinder = element(by.css('select#order-shopAccount'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setTotalInput(total) {
    await this.totalInput.sendKeys(total);
  }

  async getTotalInput() {
    return this.totalInput.getAttribute('value');
  }

  async setOrderDateInput(orderDate) {
    await this.orderDateInput.sendKeys(orderDate);
  }

  async getOrderDateInput() {
    return this.orderDateInput.getAttribute('value');
  }

  async setDeliveryDateInput(deliveryDate) {
    await this.deliveryDateInput.sendKeys(deliveryDate);
  }

  async getDeliveryDateInput() {
    return this.deliveryDateInput.getAttribute('value');
  }

  async shopAccountSelectLastOption() {
    await this.shopAccountSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shopAccountSelectOption(option) {
    await this.shopAccountSelect.sendKeys(option);
  }

  getShopAccountSelect() {
    return this.shopAccountSelect;
  }

  async getShopAccountSelectedOption() {
    return this.shopAccountSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
