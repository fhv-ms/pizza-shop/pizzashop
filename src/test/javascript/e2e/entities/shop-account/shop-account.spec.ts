import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ShopAccountComponentsPage, { ShopAccountDeleteDialog } from './shop-account.page-object';
import ShopAccountUpdatePage from './shop-account-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('ShopAccount e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shopAccountComponentsPage: ShopAccountComponentsPage;
  let shopAccountUpdatePage: ShopAccountUpdatePage;
  let shopAccountDeleteDialog: ShopAccountDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load ShopAccounts', async () => {
    await navBarPage.getEntityPage('shop-account');
    shopAccountComponentsPage = new ShopAccountComponentsPage();
    expect(await shopAccountComponentsPage.getTitle().getText()).to.match(/Shop Accounts/);
  });

  it('should load create ShopAccount page', async () => {
    await shopAccountComponentsPage.clickOnCreateButton();
    shopAccountUpdatePage = new ShopAccountUpdatePage();
    expect(await shopAccountUpdatePage.getPageTitle().getAttribute('id')).to.match(/pizzashopApp.shopAccount.home.createOrEditLabel/);
    await shopAccountUpdatePage.cancel();
  });

  it('should create and save ShopAccounts', async () => {
    async function createShopAccount() {
      await shopAccountComponentsPage.clickOnCreateButton();
      await shopAccountUpdatePage.setCreatedInput('01-01-2001');
      expect(await shopAccountUpdatePage.getCreatedInput()).to.eq('2001-01-01');
      await shopAccountUpdatePage.userSelectLastOption();
      await shopAccountUpdatePage.addressSelectLastOption();
      await waitUntilDisplayed(shopAccountUpdatePage.getSaveButton());
      await shopAccountUpdatePage.save();
      await waitUntilHidden(shopAccountUpdatePage.getSaveButton());
      expect(await shopAccountUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createShopAccount();
    await shopAccountComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await shopAccountComponentsPage.countDeleteButtons();
    await createShopAccount();

    await shopAccountComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await shopAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last ShopAccount', async () => {
    await shopAccountComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await shopAccountComponentsPage.countDeleteButtons();
    await shopAccountComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    shopAccountDeleteDialog = new ShopAccountDeleteDialog();
    expect(await shopAccountDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/pizzashopApp.shopAccount.delete.question/);
    await shopAccountDeleteDialog.clickOnConfirmButton();

    await shopAccountComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await shopAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
