import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import PizzaComponentsPage, { PizzaDeleteDialog } from './pizza.page-object';
import PizzaUpdatePage from './pizza-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Pizza e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let pizzaComponentsPage: PizzaComponentsPage;
  let pizzaUpdatePage: PizzaUpdatePage;
  let pizzaDeleteDialog: PizzaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Pizzas', async () => {
    await navBarPage.getEntityPage('pizza');
    pizzaComponentsPage = new PizzaComponentsPage();
    expect(await pizzaComponentsPage.getTitle().getText()).to.match(/Pizzas/);
  });

  it('should load create Pizza page', async () => {
    await pizzaComponentsPage.clickOnCreateButton();
    pizzaUpdatePage = new PizzaUpdatePage();
    expect(await pizzaUpdatePage.getPageTitle().getAttribute('id')).to.match(/pizzashopApp.pizza.home.createOrEditLabel/);
    await pizzaUpdatePage.cancel();
  });

  it('should create and save Pizzas', async () => {
    async function createPizza() {
      await pizzaComponentsPage.clickOnCreateButton();
      await pizzaUpdatePage.setNameInput('name');
      expect(await pizzaUpdatePage.getNameInput()).to.match(/name/);
      await pizzaUpdatePage.setPriceInput('5');
      expect(await pizzaUpdatePage.getPriceInput()).to.eq('5');
      // pizzaUpdatePage.toppingSelectLastOption();
      await pizzaUpdatePage.orderSelectLastOption();
      await waitUntilDisplayed(pizzaUpdatePage.getSaveButton());
      await pizzaUpdatePage.save();
      await waitUntilHidden(pizzaUpdatePage.getSaveButton());
      expect(await pizzaUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createPizza();
    await pizzaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await pizzaComponentsPage.countDeleteButtons();
    await createPizza();

    await pizzaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await pizzaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Pizza', async () => {
    await pizzaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await pizzaComponentsPage.countDeleteButtons();
    await pizzaComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    pizzaDeleteDialog = new PizzaDeleteDialog();
    expect(await pizzaDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/pizzashopApp.pizza.delete.question/);
    await pizzaDeleteDialog.clickOnConfirmButton();

    await pizzaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await pizzaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
