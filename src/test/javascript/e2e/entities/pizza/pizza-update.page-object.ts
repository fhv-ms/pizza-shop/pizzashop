import { element, by, ElementFinder } from 'protractor';

export default class PizzaUpdatePage {
  pageTitle: ElementFinder = element(by.id('pizzashopApp.pizza.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#pizza-name'));
  priceInput: ElementFinder = element(by.css('input#pizza-price'));
  toppingSelect: ElementFinder = element(by.css('select#pizza-topping'));
  orderSelect: ElementFinder = element(by.css('select#pizza-order'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setPriceInput(price) {
    await this.priceInput.sendKeys(price);
  }

  async getPriceInput() {
    return this.priceInput.getAttribute('value');
  }

  async toppingSelectLastOption() {
    await this.toppingSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async toppingSelectOption(option) {
    await this.toppingSelect.sendKeys(option);
  }

  getToppingSelect() {
    return this.toppingSelect;
  }

  async getToppingSelectedOption() {
    return this.toppingSelect.element(by.css('option:checked')).getText();
  }

  async orderSelectLastOption() {
    await this.orderSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async orderSelectOption(option) {
    await this.orderSelect.sendKeys(option);
  }

  getOrderSelect() {
    return this.orderSelect;
  }

  async getOrderSelectedOption() {
    return this.orderSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
