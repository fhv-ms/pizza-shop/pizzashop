import { IShopAccount } from 'app/shared/model/shop-account.model';

export interface IAddress {
  id?: number;
  street?: string;
  city?: string;
  shopAccount?: IShopAccount;
}

export const defaultValue: Readonly<IAddress> = {};
