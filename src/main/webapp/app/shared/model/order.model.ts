import { Moment } from 'moment';
import { IPizza } from 'app/shared/model/pizza.model';
import { IShopAccount } from 'app/shared/model/shop-account.model';

export interface IOrder {
  id?: number;
  total?: number;
  orderDate?: Moment;
  deliveryDate?: Moment;
  pizzas?: IPizza[];
  shopAccount?: IShopAccount;
}

export const defaultValue: Readonly<IOrder> = {};
