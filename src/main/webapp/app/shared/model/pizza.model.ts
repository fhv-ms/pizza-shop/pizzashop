import { ITopping } from 'app/shared/model/topping.model';
import { IOrder } from 'app/shared/model/order.model';

export interface IPizza {
  id?: number;
  name?: string;
  price?: number;
  toppings?: ITopping[];
  order?: IOrder;
}

export const defaultValue: Readonly<IPizza> = {};
