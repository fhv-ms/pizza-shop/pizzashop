import { Moment } from 'moment';
import { IUser } from 'app/shared/model/user.model';
import { IAddress } from 'app/shared/model/address.model';
import { IOrder } from 'app/shared/model/order.model';

export interface IShopAccount {
  id?: number;
  created?: Moment;
  user?: IUser;
  address?: IAddress;
  orders?: IOrder[];
}

export const defaultValue: Readonly<IShopAccount> = {};
