import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPizza, defaultValue } from 'app/shared/model/pizza.model';

export const ACTION_TYPES = {
  FETCH_PIZZA_LIST: 'pizza/FETCH_PIZZA_LIST',
  FETCH_PIZZA: 'pizza/FETCH_PIZZA',
  CREATE_PIZZA: 'pizza/CREATE_PIZZA',
  UPDATE_PIZZA: 'pizza/UPDATE_PIZZA',
  DELETE_PIZZA: 'pizza/DELETE_PIZZA',
  RESET: 'pizza/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPizza>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type PizzaState = Readonly<typeof initialState>;

// Reducer

export default (state: PizzaState = initialState, action): PizzaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PIZZA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PIZZA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PIZZA):
    case REQUEST(ACTION_TYPES.UPDATE_PIZZA):
    case REQUEST(ACTION_TYPES.DELETE_PIZZA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PIZZA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PIZZA):
    case FAILURE(ACTION_TYPES.CREATE_PIZZA):
    case FAILURE(ACTION_TYPES.UPDATE_PIZZA):
    case FAILURE(ACTION_TYPES.DELETE_PIZZA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PIZZA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PIZZA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PIZZA):
    case SUCCESS(ACTION_TYPES.UPDATE_PIZZA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PIZZA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/pizzas';

// Actions

export const getEntities: ICrudGetAllAction<IPizza> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PIZZA_LIST,
  payload: axios.get<IPizza>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IPizza> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PIZZA,
    payload: axios.get<IPizza>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPizza> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PIZZA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPizza> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PIZZA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPizza> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PIZZA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
