import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ITopping } from 'app/shared/model/topping.model';
import { getEntities as getToppings } from 'app/entities/topping/topping.reducer';
import { IOrder } from 'app/shared/model/order.model';
import { getEntities as getOrders } from 'app/entities/order/order.reducer';
import { getEntity, updateEntity, createEntity, reset } from './pizza.reducer';
import { IPizza } from 'app/shared/model/pizza.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPizzaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPizzaUpdateState {
  isNew: boolean;
  idstopping: any[];
  orderId: string;
}

export class PizzaUpdate extends React.Component<IPizzaUpdateProps, IPizzaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idstopping: [],
      orderId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getToppings();
    this.props.getOrders();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { pizzaEntity } = this.props;
      const entity = {
        ...pizzaEntity,
        ...values,
        toppings: mapIdList(values.toppings)
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/pizza');
  };

  render() {
    const { pizzaEntity, toppings, orders, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="pizzashopApp.pizza.home.createOrEditLabel">
              <Translate contentKey="pizzashopApp.pizza.home.createOrEditLabel">Create or edit a Pizza</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : pizzaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="pizza-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="pizza-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="pizza-name">
                    <Translate contentKey="pizzashopApp.pizza.name">Name</Translate>
                  </Label>
                  <AvField
                    id="pizza-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="priceLabel" for="pizza-price">
                    <Translate contentKey="pizzashopApp.pizza.price">Price</Translate>
                  </Label>
                  <AvField
                    id="pizza-price"
                    type="string"
                    className="form-control"
                    name="price"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="pizza-topping">
                    <Translate contentKey="pizzashopApp.pizza.topping">Topping</Translate>
                  </Label>
                  <AvInput
                    id="pizza-topping"
                    type="select"
                    multiple
                    className="form-control"
                    name="toppings"
                    value={pizzaEntity.toppings && pizzaEntity.toppings.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {toppings
                      ? toppings.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="pizza-order">
                    <Translate contentKey="pizzashopApp.pizza.order">Order</Translate>
                  </Label>
                  <AvInput id="pizza-order" type="select" className="form-control" name="order.id">
                    <option value="" key="0" />
                    {orders
                      ? orders.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/pizza" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  toppings: storeState.topping.entities,
  orders: storeState.order.entities,
  pizzaEntity: storeState.pizza.entity,
  loading: storeState.pizza.loading,
  updating: storeState.pizza.updating,
  updateSuccess: storeState.pizza.updateSuccess
});

const mapDispatchToProps = {
  getToppings,
  getOrders,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PizzaUpdate);
