import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pizza.reducer';
import { IPizza } from 'app/shared/model/pizza.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPizzaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PizzaDetail extends React.Component<IPizzaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { pizzaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="pizzashopApp.pizza.detail.title">Pizza</Translate> [<b>{pizzaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="pizzashopApp.pizza.name">Name</Translate>
              </span>
            </dt>
            <dd>{pizzaEntity.name}</dd>
            <dt>
              <span id="price">
                <Translate contentKey="pizzashopApp.pizza.price">Price</Translate>
              </span>
            </dt>
            <dd>{pizzaEntity.price}</dd>
            <dt>
              <Translate contentKey="pizzashopApp.pizza.topping">Topping</Translate>
            </dt>
            <dd>
              {pizzaEntity.toppings
                ? pizzaEntity.toppings.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.id}</a>
                      {i === pizzaEntity.toppings.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
            <dt>
              <Translate contentKey="pizzashopApp.pizza.order">Order</Translate>
            </dt>
            <dd>{pizzaEntity.order ? pizzaEntity.order.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/pizza" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/pizza/${pizzaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pizza }: IRootState) => ({
  pizzaEntity: pizza.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PizzaDetail);
