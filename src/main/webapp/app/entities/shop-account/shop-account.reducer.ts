import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IShopAccount, defaultValue } from 'app/shared/model/shop-account.model';

export const ACTION_TYPES = {
  FETCH_SHOPACCOUNT_LIST: 'shopAccount/FETCH_SHOPACCOUNT_LIST',
  FETCH_SHOPACCOUNT: 'shopAccount/FETCH_SHOPACCOUNT',
  CREATE_SHOPACCOUNT: 'shopAccount/CREATE_SHOPACCOUNT',
  UPDATE_SHOPACCOUNT: 'shopAccount/UPDATE_SHOPACCOUNT',
  DELETE_SHOPACCOUNT: 'shopAccount/DELETE_SHOPACCOUNT',
  RESET: 'shopAccount/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IShopAccount>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ShopAccountState = Readonly<typeof initialState>;

// Reducer

export default (state: ShopAccountState = initialState, action): ShopAccountState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SHOPACCOUNT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SHOPACCOUNT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SHOPACCOUNT):
    case REQUEST(ACTION_TYPES.UPDATE_SHOPACCOUNT):
    case REQUEST(ACTION_TYPES.DELETE_SHOPACCOUNT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SHOPACCOUNT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SHOPACCOUNT):
    case FAILURE(ACTION_TYPES.CREATE_SHOPACCOUNT):
    case FAILURE(ACTION_TYPES.UPDATE_SHOPACCOUNT):
    case FAILURE(ACTION_TYPES.DELETE_SHOPACCOUNT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOPACCOUNT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOPACCOUNT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SHOPACCOUNT):
    case SUCCESS(ACTION_TYPES.UPDATE_SHOPACCOUNT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SHOPACCOUNT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/shop-accounts';

// Actions

export const getEntities: ICrudGetAllAction<IShopAccount> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SHOPACCOUNT_LIST,
  payload: axios.get<IShopAccount>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IShopAccount> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SHOPACCOUNT,
    payload: axios.get<IShopAccount>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IShopAccount> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SHOPACCOUNT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IShopAccount> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SHOPACCOUNT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IShopAccount> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SHOPACCOUNT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
