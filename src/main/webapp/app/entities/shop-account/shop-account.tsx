import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './shop-account.reducer';
import { IShopAccount } from 'app/shared/model/shop-account.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IShopAccountProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class ShopAccount extends React.Component<IShopAccountProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { shopAccountList, match } = this.props;
    return (
      <div>
        <h2 id="shop-account-heading">
          <Translate contentKey="pizzashopApp.shopAccount.home.title">Shop Accounts</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="pizzashopApp.shopAccount.home.createLabel">Create a new Shop Account</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          {shopAccountList && shopAccountList.length > 0 ? (
            <Table responsive aria-describedby="shop-account-heading">
              <thead>
                <tr>
                  <th>
                    <Translate contentKey="global.field.id">ID</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.shopAccount.created">Created</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.shopAccount.user">User</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.shopAccount.address">Address</Translate>
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {shopAccountList.map((shopAccount, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${shopAccount.id}`} color="link" size="sm">
                        {shopAccount.id}
                      </Button>
                    </td>
                    <td>
                      <TextFormat type="date" value={shopAccount.created} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>{shopAccount.user ? shopAccount.user.id : ''}</td>
                    <td>{shopAccount.address ? <Link to={`address/${shopAccount.address.id}`}>{shopAccount.address.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${shopAccount.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${shopAccount.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${shopAccount.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">
              <Translate contentKey="pizzashopApp.shopAccount.home.notFound">No Shop Accounts found</Translate>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ shopAccount }: IRootState) => ({
  shopAccountList: shopAccount.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShopAccount);
