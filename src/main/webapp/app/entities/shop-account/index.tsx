import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ShopAccount from './shop-account';
import ShopAccountDetail from './shop-account-detail';
import ShopAccountUpdate from './shop-account-update';
import ShopAccountDeleteDialog from './shop-account-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ShopAccountUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ShopAccountUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ShopAccountDetail} />
      <ErrorBoundaryRoute path={match.url} component={ShopAccount} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ShopAccountDeleteDialog} />
  </>
);

export default Routes;
