import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './order.reducer';
import { IOrder } from 'app/shared/model/order.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOrderDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class OrderDetail extends React.Component<IOrderDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { orderEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="pizzashopApp.order.detail.title">Order</Translate> [<b>{orderEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="total">
                <Translate contentKey="pizzashopApp.order.total">Total</Translate>
              </span>
            </dt>
            <dd>{orderEntity.total}</dd>
            <dt>
              <span id="orderDate">
                <Translate contentKey="pizzashopApp.order.orderDate">Order Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={orderEntity.orderDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="deliveryDate">
                <Translate contentKey="pizzashopApp.order.deliveryDate">Delivery Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={orderEntity.deliveryDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="pizzashopApp.order.shopAccount">Shop Account</Translate>
            </dt>
            <dd>{orderEntity.shopAccount ? orderEntity.shopAccount.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/order" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/order/${orderEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ order }: IRootState) => ({
  orderEntity: order.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetail);
