import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Topping from './topping';
import ToppingDetail from './topping-detail';
import ToppingUpdate from './topping-update';
import ToppingDeleteDialog from './topping-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ToppingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ToppingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ToppingDetail} />
      <ErrorBoundaryRoute path={match.url} component={Topping} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ToppingDeleteDialog} />
  </>
);

export default Routes;
