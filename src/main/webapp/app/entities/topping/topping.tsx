import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './topping.reducer';
import { ITopping } from 'app/shared/model/topping.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IToppingProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Topping extends React.Component<IToppingProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { toppingList, match } = this.props;
    return (
      <div>
        <h2 id="topping-heading">
          <Translate contentKey="pizzashopApp.topping.home.title">Toppings</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="pizzashopApp.topping.home.createLabel">Create a new Topping</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          {toppingList && toppingList.length > 0 ? (
            <Table responsive aria-describedby="topping-heading">
              <thead>
                <tr>
                  <th>
                    <Translate contentKey="global.field.id">ID</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.topping.name">Name</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.topping.price">Price</Translate>
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {toppingList.map((topping, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${topping.id}`} color="link" size="sm">
                        {topping.id}
                      </Button>
                    </td>
                    <td>{topping.name}</td>
                    <td>{topping.price}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${topping.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${topping.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${topping.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">
              <Translate contentKey="pizzashopApp.topping.home.notFound">No Toppings found</Translate>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ topping }: IRootState) => ({
  toppingList: topping.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Topping);
