import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITopping, defaultValue } from 'app/shared/model/topping.model';

export const ACTION_TYPES = {
  FETCH_TOPPING_LIST: 'topping/FETCH_TOPPING_LIST',
  FETCH_TOPPING: 'topping/FETCH_TOPPING',
  CREATE_TOPPING: 'topping/CREATE_TOPPING',
  UPDATE_TOPPING: 'topping/UPDATE_TOPPING',
  DELETE_TOPPING: 'topping/DELETE_TOPPING',
  RESET: 'topping/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITopping>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ToppingState = Readonly<typeof initialState>;

// Reducer

export default (state: ToppingState = initialState, action): ToppingState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TOPPING_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TOPPING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_TOPPING):
    case REQUEST(ACTION_TYPES.UPDATE_TOPPING):
    case REQUEST(ACTION_TYPES.DELETE_TOPPING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_TOPPING_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TOPPING):
    case FAILURE(ACTION_TYPES.CREATE_TOPPING):
    case FAILURE(ACTION_TYPES.UPDATE_TOPPING):
    case FAILURE(ACTION_TYPES.DELETE_TOPPING):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOPPING_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOPPING):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_TOPPING):
    case SUCCESS(ACTION_TYPES.UPDATE_TOPPING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_TOPPING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/toppings';

// Actions

export const getEntities: ICrudGetAllAction<ITopping> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_TOPPING_LIST,
  payload: axios.get<ITopping>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ITopping> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TOPPING,
    payload: axios.get<ITopping>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ITopping> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TOPPING,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITopping> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TOPPING,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITopping> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TOPPING,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
