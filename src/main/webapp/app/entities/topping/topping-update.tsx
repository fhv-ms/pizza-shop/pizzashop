import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPizza } from 'app/shared/model/pizza.model';
import { getEntities as getPizzas } from 'app/entities/pizza/pizza.reducer';
import { getEntity, updateEntity, createEntity, reset } from './topping.reducer';
import { ITopping } from 'app/shared/model/topping.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IToppingUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IToppingUpdateState {
  isNew: boolean;
  pizzaId: string;
}

export class ToppingUpdate extends React.Component<IToppingUpdateProps, IToppingUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      pizzaId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPizzas();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { toppingEntity } = this.props;
      const entity = {
        ...toppingEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/topping');
  };

  render() {
    const { toppingEntity, pizzas, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="pizzashopApp.topping.home.createOrEditLabel">
              <Translate contentKey="pizzashopApp.topping.home.createOrEditLabel">Create or edit a Topping</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : toppingEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="topping-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="topping-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="topping-name">
                    <Translate contentKey="pizzashopApp.topping.name">Name</Translate>
                  </Label>
                  <AvField
                    id="topping-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="priceLabel" for="topping-price">
                    <Translate contentKey="pizzashopApp.topping.price">Price</Translate>
                  </Label>
                  <AvField
                    id="topping-price"
                    type="string"
                    className="form-control"
                    name="price"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/topping" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  pizzas: storeState.pizza.entities,
  toppingEntity: storeState.topping.entity,
  loading: storeState.topping.loading,
  updating: storeState.topping.updating,
  updateSuccess: storeState.topping.updateSuccess
});

const mapDispatchToProps = {
  getPizzas,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToppingUpdate);
