/**
 * Data Access Objects used by WebSocket services.
 */
package at.fhv.pizzashop.web.websocket.dto;
