package at.fhv.pizzashop.web.rest;

import at.fhv.pizzashop.domain.Topping;
import at.fhv.pizzashop.repository.ToppingRepository;
import at.fhv.pizzashop.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.fhv.pizzashop.domain.Topping}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ToppingResource {

    private final Logger log = LoggerFactory.getLogger(ToppingResource.class);

    private static final String ENTITY_NAME = "topping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ToppingRepository toppingRepository;

    public ToppingResource(ToppingRepository toppingRepository) {
        this.toppingRepository = toppingRepository;
    }

    /**
     * {@code POST  /toppings} : Create a new topping.
     *
     * @param topping the topping to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new topping, or with status {@code 400 (Bad Request)} if the topping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/toppings")
    public ResponseEntity<Topping> createTopping(@Valid @RequestBody Topping topping) throws URISyntaxException {
        log.debug("REST request to save Topping : {}", topping);
        if (topping.getId() != null) {
            throw new BadRequestAlertException("A new topping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Topping result = toppingRepository.save(topping);
        return ResponseEntity.created(new URI("/api/toppings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /toppings} : Updates an existing topping.
     *
     * @param topping the topping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated topping,
     * or with status {@code 400 (Bad Request)} if the topping is not valid,
     * or with status {@code 500 (Internal Server Error)} if the topping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/toppings")
    public ResponseEntity<Topping> updateTopping(@Valid @RequestBody Topping topping) throws URISyntaxException {
        log.debug("REST request to update Topping : {}", topping);
        if (topping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Topping result = toppingRepository.save(topping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, topping.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /toppings} : get all the toppings.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of toppings in body.
     */
    @GetMapping("/toppings")
    public List<Topping> getAllToppings() {
        log.debug("REST request to get all Toppings");
        return toppingRepository.findAll();
    }

    /**
     * {@code GET  /toppings/:id} : get the "id" topping.
     *
     * @param id the id of the topping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the topping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/toppings/{id}")
    public ResponseEntity<Topping> getTopping(@PathVariable Long id) {
        log.debug("REST request to get Topping : {}", id);
        Optional<Topping> topping = toppingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(topping);
    }

    /**
     * {@code DELETE  /toppings/:id} : delete the "id" topping.
     *
     * @param id the id of the topping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/toppings/{id}")
    public ResponseEntity<Void> deleteTopping(@PathVariable Long id) {
        log.debug("REST request to delete Topping : {}", id);
        toppingRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
