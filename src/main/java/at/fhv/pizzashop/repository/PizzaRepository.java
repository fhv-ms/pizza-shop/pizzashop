package at.fhv.pizzashop.repository;
import at.fhv.pizzashop.domain.Pizza;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Pizza entity.
 */
@Repository
public interface PizzaRepository extends JpaRepository<Pizza, Long> {

    @Query(value = "select distinct pizza from Pizza pizza left join fetch pizza.toppings",
        countQuery = "select count(distinct pizza) from Pizza pizza")
    Page<Pizza> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct pizza from Pizza pizza left join fetch pizza.toppings")
    List<Pizza> findAllWithEagerRelationships();

    @Query("select pizza from Pizza pizza left join fetch pizza.toppings where pizza.id =:id")
    Optional<Pizza> findOneWithEagerRelationships(@Param("id") Long id);

}
