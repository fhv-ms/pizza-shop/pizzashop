package at.fhv.pizzashop.repository;
import at.fhv.pizzashop.domain.Topping;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Topping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ToppingRepository extends JpaRepository<Topping, Long> {

}
