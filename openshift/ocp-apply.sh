#!/usr/bin/env sh
# Use this script to run oc commands to create resources in the selected namespace. 
# Before executing the script make sure you have logged in 'oc login' to your OpenShift cluster
# Files are ordered in proper order. 'oc process' processes the template as resources which is again piped to
# 'oc apply' to create those resources in OpenShift namespace
oc new-project pizza-shop
oc process -f registry/scc-config.yml | oc apply -f -
oc process -f pizzashop/pizzashop-mysql.yml | oc apply -f -
oc process -f pizzashop/pizzashop-deployment.yml | oc apply -f -
